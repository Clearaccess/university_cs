package university;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;
import java.util.Queue;
import java.util.Scanner;
import university.until.FilesFinder;
import university.until.MagicNumberExtractor;

/*TODO
  1. Work with console (get file, get folder, print list of copy files paths)
  ConsoleHandler
  MagicNumberExtractor
  FilesFinder
*/

public class App {

  protected static final String OPTIONS_PATH = "options.properties";
  protected static final String OPTIONS_APP_CONSOLE = "app.console";
  protected static final String OPTIONS_FILE_PATH = "app.file_path";
  protected static final String OPTIONS_FOLDER_PATH = "app.folder_path";
  protected static final String OPTIONS_MAGIC_NUMBER_FIRST_BYTE = "app.magic_number_first_byte";
  protected static final String OPTIONS_MAGIC_NUMBER_BYTES = "app.magic_number_bytes";

  protected Properties options;
  protected Path file;
  protected Path folder;
  protected int firstByte;
  protected int bytes;


  public static void main(String[] args) throws IOException, URISyntaxException {

    App app = new App();
    app.init();
    app.find();

  }

  public void init() throws IOException, URISyntaxException {

    options = loadProperties();

    if (Boolean.parseBoolean(options.getProperty(OPTIONS_APP_CONSOLE))) {
      System.out.println("Console init");
      initConsole();
    } else {
      System.out.println("Properties init");
      initProperties();
    }
  }

  public void find() throws IOException {

    System.out.println("Start searching");
    byte[] magicNumber = MagicNumberExtractor.extract(file, firstByte, bytes);
    Queue<String> similarFilesPaths = FilesFinder.findAll(folder, magicNumber, firstByte, bytes);

    System.out.println("All scanned files: "+FilesFinder.getAmountAnalyzedFiles());
    System.out.println("Similar files found: " + similarFilesPaths.size());
    similarFilesPaths.forEach(System.out::println);
  }

  private Properties loadProperties() throws URISyntaxException, IOException {

    ClassLoader loader = Thread.currentThread().getContextClassLoader();

    URL propertyURL = loader.getResource(OPTIONS_PATH);
    Path propertyPath = Paths.get(propertyURL.toURI());

    Properties properties = new Properties();
    byte[] bytes = Files.readAllBytes(propertyPath);
    String content = new String(bytes, "windows-1251");
    properties.load(new StringReader(content));

    return properties;
  }

  private void initConsole() throws UnsupportedEncodingException {
    Scanner in = new Scanner(System.in);

    System.out.println("Enter path to file:");
    String fileURI = in.nextLine();
    file = Paths.get(URLDecoder.decode(fileURI, "UTF-8"));
    if (Files.notExists(file)) {
      System.err.println("File not found");
      return;
    }

    System.out.println("Enter path to folder:");
    String folderURI = in.nextLine();
    folder = Paths.get(URLDecoder.decode(folderURI, "UTF-8"));
    if (Files.notExists(folder) || !Files.isDirectory(folder)) {
      System.err.println("Folder not found");
      return;
    }
  }

  private void initProperties() throws UnsupportedEncodingException, FileNotFoundException {

    String fileURI = options.getProperty(OPTIONS_FILE_PATH);
    file = Paths.get(URLDecoder.decode(fileURI, "UTF-8"));
    if (Files.notExists(file)) {
      System.err.println("File not found");
      throw new FileNotFoundException();
    }

    String folderURI = options.getProperty(OPTIONS_FOLDER_PATH);
    folder = Paths.get(URLDecoder.decode(folderURI, "UTF-8"));
    if (Files.notExists(folder) || !Files.isDirectory(folder)) {
      System.err.println("Folder not found");
      throw new FileNotFoundException();
    }

    firstByte = Integer.parseInt(options.getProperty(OPTIONS_MAGIC_NUMBER_FIRST_BYTE));
    bytes = Integer.parseInt(options.getProperty(OPTIONS_MAGIC_NUMBER_BYTES));
  }
}
