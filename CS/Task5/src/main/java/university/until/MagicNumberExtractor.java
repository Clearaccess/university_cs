package university.until;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channel;
import java.nio.channels.FileChannel;
import java.nio.file.FileStore;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

public class MagicNumberExtractor {

  public static byte[] extract(Path file, int firstByte, int bytes) throws IOException {

    FileInputStream in = new FileInputStream(file.toFile());
    FileChannel ch = in.getChannel();
    ByteBuffer magicNumber = ByteBuffer.allocate(bytes);

    ch.read(magicNumber, firstByte);
    return magicNumber.array();
  }

  public static byte[] extract(Path file) throws IOException {

    return Files.readAllBytes(file);
  }
}
