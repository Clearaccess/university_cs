package university.until;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class FilesFinder {

  protected static final long CRITICAL_SIZE = 64L*1024L*1024L;

  private static int amountAnalyzedFiles=0;

  public static int getAmountAnalyzedFiles() {
    return amountAnalyzedFiles;
  }

  public static Queue<String> findAll(Path folder, byte[] magicNumber, int firstByte, int bytes) {

    resetAmountAnalyzedFiles();
    Queue<String> paths = new ConcurrentLinkedQueue<>();
    find(folder, magicNumber, firstByte, bytes, paths);
    return paths;
  }

  private static void find(Path file, byte[] magicNumber, int firstByte, int bytes, Queue<String> paths) {

    if (!Files.isDirectory(file)) {

      incAmountAnalyzedFiles();

      File f = file.toFile();

      if (f.length()<=CRITICAL_SIZE && isSimilarFile(file, magicNumber, firstByte, bytes)) {
        paths.add(file.toAbsolutePath().toString());
      }

      if(f.length()>CRITICAL_SIZE){
        System.out.println(f.getAbsolutePath()+" "+ f.length());
      }

      return;
    }

    try {
      Files.list(file).parallel().forEach(f ->
          find(f, magicNumber, firstByte, bytes, paths));
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  private static boolean isSimilarFile(Path file, byte[] magicNumber, int firstByte, int bytes) {

    try {
      byte[] data = MagicNumberExtractor.extract(file);
      return toBinaryArray(data).contains(toBinaryArray(magicNumber));
    } catch (IOException e) {
      e.printStackTrace();
      return false;
    }
  }

  private static String toBinaryArray(byte[] bytes){

    StringBuilder out = new StringBuilder();
    for(byte b: bytes){
      out.append(toBinaryByte(b));
    }

    return out.toString();
  }

  private static String toBinaryByte(byte symbol){

    String binary = Integer.toBinaryString(Byte.toUnsignedInt(symbol));
    return (binary.length()!=8? String.join("", Collections.nCopies(8-binary.length(),"0"))+ binary: binary);
  }

  private static void incAmountAnalyzedFiles(){

    amountAnalyzedFiles++;
  }

  private static void resetAmountAnalyzedFiles(){

    amountAnalyzedFiles = 0;
  }
}
