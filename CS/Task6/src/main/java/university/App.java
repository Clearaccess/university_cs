package university;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.Properties;
import university.command.AbstractCommandManager;
import university.command.AbstractCommandManager.Commands;
import university.command.impl.CommandManagerImpl;
import university.console.AbstractConsoleHandler;
import university.console.impl.ConsoleHandlerImpl;

/*
  COMMANDS:
    encode - encode text
    decode - decode text
    exit - exit from app
   */

public class App {

  protected static final String OPTIONS_PATH = "options.properties";
  protected static final String OPTIONS_CONTAINER_PATH = "container_path";
  protected static final String OPTIONS_CHARSET = "charset";
  protected static final String ALPHABET = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";

  protected static Properties options;
  protected static AbstractCommandManager commandManager;
  protected static AbstractConsoleHandler consoleHandler;

  public static void main(String[] args) {

    try {

      init();
      while(true) {

        consoleHandler.println("Enter command ('encode', 'decode' or 'exit')...");
        String cmd = consoleHandler.readLine();
        Commands commandName = Commands.commandOf(cmd);
        commandManager.executeCommand(commandName);
      }
    } catch (IOException e) {
      e.printStackTrace();
    } catch (URISyntaxException e) {
      e.printStackTrace();
    }
  }

  private static void init() throws IOException, URISyntaxException {
    options = loadProperties();
    consoleHandler = new ConsoleHandlerImpl();
    commandManager = initCommandManager();
  }

  private static Properties loadProperties() throws URISyntaxException, IOException {

    ClassLoader loader = Thread.currentThread().getContextClassLoader();
    Properties properties = new Properties();

    try(InputStream resourceStream = loader.getResourceAsStream(OPTIONS_PATH)) {
      properties.load(resourceStream);
    }

    return properties;
  }

  private static AbstractCommandManager initCommandManager(){

    return new CommandManagerImpl(consoleHandler, ALPHABET);
  }
}
