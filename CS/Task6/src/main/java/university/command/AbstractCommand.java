package university.command;

import java.io.IOException;
import university.console.AbstractConsoleHandler;

public abstract class AbstractCommand {

  protected AbstractConsoleHandler consoleHandler;
  protected String alphabet;

  public AbstractCommand(AbstractConsoleHandler consoleHandler, String alphabet) {

    this.consoleHandler = consoleHandler;
    this.alphabet = alphabet;
  }

  public abstract void doIt() throws IOException;
}
