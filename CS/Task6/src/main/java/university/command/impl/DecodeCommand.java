package university.command.impl;

import java.io.IOException;
import university.command.AbstractCommand;
import university.console.AbstractConsoleHandler;
import university.util.Decoder;
import university.util.impl.DecoderImpl;

public class DecodeCommand extends AbstractCommand {

  public DecodeCommand(AbstractConsoleHandler consoleHandler, String alphabet) {
    super(consoleHandler, alphabet);
  }

  @Override
  public void doIt() throws IOException {

    consoleHandler.println("Enter decoding text: ");
    String text = consoleHandler.readLine();
    if (text == null) {
      System.err.println("Incorrect text");
      return;
    }

    consoleHandler.println("Enter key: ");
    String key = consoleHandler.readLine();
    if (key == null) {
      System.err.println("Incorrect key");
      return;
    }

    Decoder decoder = new DecoderImpl(alphabet);
    String decodedText = decoder.decode(text.toUpperCase(), key.toUpperCase());
    consoleHandler.println("Decoded text: " + decodedText);
  }
}
