package university.command.impl;

import java.io.IOException;
import university.command.AbstractCommand;
import university.console.AbstractConsoleHandler;

public class ExitCommand extends AbstractCommand {

  public ExitCommand(AbstractConsoleHandler consoleHandler, String alphabet) {
    super(consoleHandler, alphabet);
  }

  @Override
  public void doIt() throws IOException {

    System.exit(0);
  }
}
