package university.command.impl;

import java.io.IOException;
import university.command.AbstractCommand;
import university.console.AbstractConsoleHandler;
import university.util.Encoder;
import university.util.impl.EncoderImpl;

public class EncodeCommand extends AbstractCommand {


  public EncodeCommand(AbstractConsoleHandler consoleHandler, String alphabet) {
    super(consoleHandler, alphabet);
  }

  @Override
  public void doIt() throws IOException {

    consoleHandler.println("Enter encoding text: ");
    String text = consoleHandler.readLine();
    if (text == null) {
      System.err.println("Incorrect text");
      return;
    }

    consoleHandler.println("Enter key: ");
    String key = consoleHandler.readLine();
    if (key == null) {
      System.err.println("Incorrect key");
      return;
    }

    Encoder encoder = new EncoderImpl(alphabet);
    String encodedText = encoder.encode(text.toUpperCase(), key.toUpperCase());
    consoleHandler.println("Encoded text: "+ encodedText);
  }
}
