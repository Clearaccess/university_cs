package university.command.impl;

import java.io.IOException;
import university.command.AbstractCommand;
import university.command.AbstractCommandManager;
import university.console.AbstractConsoleHandler;

public class CommandManagerImpl extends AbstractCommandManager {

  public CommandManagerImpl(AbstractConsoleHandler consoleHandler, String alphabet) {

    super(consoleHandler, alphabet);
  }

  @Override
  public void executeCommand(Commands command) throws IOException {

    AbstractCommand activeCommand = null;

    if (command.equals(Commands.ENCODE)) {

      activeCommand = new EncodeCommand(consoleHandler, alphabet);

    } else if (command.equals(Commands.DECODE)) {

      activeCommand = new DecodeCommand(consoleHandler, alphabet);

    } else {

      activeCommand = new ExitCommand(consoleHandler, alphabet);
    }

    activeCommand.doIt();
  }
}
