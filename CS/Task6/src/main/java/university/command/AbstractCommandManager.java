package university.command;

import java.io.IOException;
import java.security.InvalidParameterException;
import university.console.AbstractConsoleHandler;

public abstract class AbstractCommandManager {

  protected AbstractConsoleHandler consoleHandler;
  protected String alphabet;

  public AbstractCommandManager(AbstractConsoleHandler consoleHandler, String alphabet) {

    this.consoleHandler = consoleHandler;
    this.alphabet = alphabet;
  }

  public abstract void executeCommand(Commands command) throws IOException;

  public static enum Commands {

    ENCODE("encode"),
    DECODE("decode"),
    EXIT("exit");

    private String command;

    Commands(String command) {
      this.command = command;
    }

    public static Commands commandOf(String command) {

      if (ENCODE.command.equals(command)) {
        return ENCODE;
      } else if (DECODE.command.equals(command)) {
        return DECODE;
      } else if (EXIT.command.equals(command)) {
        return EXIT;
      }

      throw new InvalidParameterException("Invalid command");
    }
  }
}
