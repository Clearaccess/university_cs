package university.util.impl;

import university.util.AbstractCoder;
import university.util.Encoder;

public class EncoderImpl extends AbstractCoder implements Encoder {

  public EncoderImpl(String alphabet) {
    super(alphabet);
  }

  @Override
  public String encode(String text, String key) {

    String temp = cut(text);
    StringBuilder encodedText = new StringBuilder();

    for(int i=0;i<temp.length();i++){

      int colInd = mappingCharToIndex.get(((int)Character.toUpperCase(temp.charAt(i))));
      int rowInd = mappingCharToIndex.get(((int)Character.toUpperCase(key.charAt(i%key.length()))));

      encodedText.append(tab[rowInd][colInd]);
    }

    return uncut(text, encodedText.toString());
  }
}
