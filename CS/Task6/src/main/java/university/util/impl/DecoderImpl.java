package university.util.impl;

import university.util.AbstractCoder;
import university.util.Decoder;

public class DecoderImpl extends AbstractCoder implements Decoder {

  public DecoderImpl(String alphabet) {
    super(alphabet);
  }

  @Override
  public String decode(String text, String key) {

    String temp = cut(text);
    StringBuilder decodedText = new StringBuilder();

    for (int i = 0; i < temp.length(); i++) {
      int rowInd = mappingCharToIndex
          .get(((int) Character.toUpperCase(key.charAt(i % key.length()))));
      int colInd = calcColInd(Character.toUpperCase(temp.charAt(i)), rowInd);
      decodedText.append(tab[0][colInd]);
    }

    return uncut(text, decodedText.toString());
  }

  private int calcColInd(char ch, int rowInd) {

    for (int i = 0; i < alphabet.length(); i++) {
      if (tab[rowInd][i] == ch) {
        return i;
      }
    }

    return -1;
  }
}