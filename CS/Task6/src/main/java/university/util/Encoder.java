package university.util;

import java.io.IOException;
import java.nio.file.Path;

public interface Encoder {

  String encode(String text, String key);
}
