package university.util;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractCoder {

  protected String alphabet;
  protected Map<Integer, Integer> mappingCharToIndex;
  protected char[][] tab;

  public AbstractCoder(String alphabet){

    this.alphabet = alphabet.toUpperCase();
    this.mappingCharToIndex = mapCharToIndex(alphabet);
    this.tab = generateTab(alphabet);
  }

  protected Map<Integer, Integer> mapCharToIndex(String alphabet){

    Map<Integer, Integer> mapping = new HashMap<>();
    for(int i=0;i<alphabet.length();i++){
      mapping.put((int) Character.toUpperCase(alphabet.charAt(i)), i);
    }

    return mapping;
  }

  protected char[][] generateTab(String alphabet){

    char[][] tab = new char[alphabet.length()][alphabet.length()];
    for(int i=0;i<alphabet.length();i++){
      for(int j=i;j<alphabet.length()+i;j++){
        tab[i][j-i] = Character.toUpperCase(alphabet.charAt(j%alphabet.length()));
      }
    }

    return tab;
  }


  protected String cut(String orgText){

    StringBuilder out = new StringBuilder();
    for (int i=0;i<orgText.length();i++){
      if(alphabet.contains(orgText.subSequence(i, i+1))){
        out.append(orgText.charAt(i));
      }
    }

    return out.toString();
  }

  protected String uncut(String orgText, String trText){

    StringBuilder out = new StringBuilder();
    for (int i=0, j=0;i<orgText.length();i++){
      if(alphabet.contains(orgText.subSequence(i,i+1))){
        out.append(trText.charAt(j));
        j++;
      } else {
        out.append(orgText.charAt(i));
      }
    }

    return out.toString();
  }
}
