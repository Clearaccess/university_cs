package university.command;

import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.Scanner;
import university.console.AbstractConsoleHandler;
import university.container.AbstractContainerManager;

public abstract class AbstractCommandManager {

  protected AbstractContainerManager containerManager;
  protected AbstractConsoleHandler consoleHandler;

  public AbstractCommandManager(AbstractContainerManager containerManager, AbstractConsoleHandler consoleHandler) {

    this.containerManager = containerManager;
    this.consoleHandler = consoleHandler;
  }

  public abstract void executeCommand(Commands command) throws IOException;

  public static enum Commands{

    ENCODE("encode"),
    DECODE("decode"),
    EXIT("exit");

    private String command;
    Commands(String command){
      this.command =command;
    }

    public static Commands commandOf(String command){

      if(ENCODE.command.equals(command)){
        return ENCODE;
      } else if (DECODE.command.equals(command)){
        return DECODE;
      } else if (EXIT.command.equals(command)){
        return EXIT;
      }

      throw new InvalidParameterException("Invalid command");
    }
  }
}
