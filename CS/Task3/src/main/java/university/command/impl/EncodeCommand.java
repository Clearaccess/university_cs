package university.command.impl;

import java.io.IOException;
import university.command.AbstractCommand;
import university.console.AbstractConsoleHandler;
import university.container.AbstractContainerManager;
import university.util.Encoder;
import university.util.impl.EncoderImpl;

public class EncodeCommand extends AbstractCommand {


  public EncodeCommand(AbstractContainerManager containerManager, AbstractConsoleHandler consoleHandler) {
    super(containerManager, consoleHandler);
  }

  @Override
  public void doIt() throws IOException {

    consoleHandler.println("Enter coding phrase: ");
    String phrase = consoleHandler.readLine();
    if (phrase == null) {
      System.err.println("Incorrect phrase");
      return;
    }

    Encoder encoder = new EncoderImpl(containerManager);
    encoder.encode(phrase);
    consoleHandler.println("Phrase was encoded");
  }
}
