package university.console.impl;

import university.console.AbstractConsoleHandler;

public class ConsoleHandlerImpl extends AbstractConsoleHandler {

  @Override
  public void println(String out) {

    System.out.println(out);
  }

  @Override
  public String readLine() {

    return scanner.nextLine();
  }
}
