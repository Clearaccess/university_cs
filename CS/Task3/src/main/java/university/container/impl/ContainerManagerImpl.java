package university.container.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.stream.Collectors;
import university.container.AbstractContainerManager;

public class ContainerManagerImpl extends AbstractContainerManager {


  public ContainerManagerImpl(String containerPath, String charsetName)
      throws IOException {
    super(containerPath, charsetName);
  }

  @Override
  public String loadAllAsLine() throws IOException {

    return new String(Files.readAllBytes(containerPath), charset);
  }

  @Override
  public List<String> loadLines() throws IOException {

    return Files.lines(containerPath, charset).collect(Collectors.toList());
  }

  @Override
  public void writeLine(String line) throws IOException {

    Files.write(containerPath, line.getBytes(charset));
  }

  @Override
  public void writeLines(List<String> lines) throws IOException {
    Files.write(containerPath, lines, charset, StandardOpenOption.WRITE);
  }

  @Override
  public void resetContainerState() throws IOException {

    List<String> lines = Files.lines(containerPath, charset)
        .map(ContainerManagerImpl::removeMultiSpace).collect(Collectors.toList());
    Files.write(containerPath, lines, charset, StandardOpenOption.WRITE);
    resetIsEncoded();
  }

  private static String removeMultiSpace(String line) {

    if (line.contains("  ")) {

      return line.replaceAll("\\s{2,}", " ");
    }

    return line;
  }
}
