package university.container;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractContainerManager {

  protected Path containerPath;
  protected Charset charset;
  protected boolean isEncoded;
  protected long dictPower;

  public AbstractContainerManager(String containerPath, String charsetName)
      throws IOException {

    this.charset = Charset.forName(charsetName);
    this.containerPath = Paths.get(URLDecoder.decode(containerPath, this.charset.name()));
    if(Files.notExists(this.containerPath)){
      throw new FileNotFoundException("Container path isn't correct");
    }
    this.dictPower = Files.lines(this.containerPath, charset).mapToLong(line -> line.chars().filter(ch->ch==' ').count()).sum();
    this.isEncoded = false;
  }

  public Charset getCharset() {
    return charset;
  }

  public long getDictPower() {
    return dictPower;
  }

  public boolean isEncoded(){
    return isEncoded;
  }

  public void setEncoded() {
    isEncoded = true;
  }

  public void resetIsEncoded(){
    isEncoded = false;
  }

  public char toCyrillicSymbol(int symbolCode){
    return new String(new byte[]{(byte)symbolCode}, charset).charAt(0);
  }

  public abstract List<String> loadLines() throws IOException;
  public abstract String loadAllAsLine() throws IOException;
  public abstract void writeLines(List<String>lines) throws IOException;
  public abstract void writeLine(String line) throws IOException;
  public abstract void resetContainerState() throws IOException;
}
