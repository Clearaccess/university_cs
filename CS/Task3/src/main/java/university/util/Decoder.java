package university.util;

import java.io.IOException;
import java.nio.file.Path;

public interface Decoder {
  String decode() throws IOException;
}
