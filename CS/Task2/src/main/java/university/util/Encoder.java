package university.util;

import java.io.IOException;
import java.nio.file.Path;

public interface Encoder {
  void encode(String phrase) throws IOException;
}
