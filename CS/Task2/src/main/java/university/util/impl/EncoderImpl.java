package university.util.impl;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import university.container.AbstractContainerManager;
import university.util.Encoder;

public class EncoderImpl implements Encoder {

  protected AbstractContainerManager containerManager;

  public EncoderImpl(AbstractContainerManager containerManager) {
    this.containerManager = containerManager;
  }

  @Override
  public void encode(String phrase) throws IOException {

    containerManager.resetContainerState();
    List<String> lines = containerManager.loadLines();
    byte[] phraseBytes = phrase.getBytes(containerManager.getCharset());
    lines = encodeToLines(lines, phraseBytes);
    containerManager.writeLines(lines);
    containerManager.setEncoded();
  }

  private List<String> encodeToLines(List<String> lines, byte[] phraseBytes){

    if(lines.size()<phraseBytes.length*8){
      throw new IllegalArgumentException("Phrase is very long for container");
    }

    int linesIndex = 0;
    for(byte symbol: phraseBytes){
      String binarySymbol = toBinaryFormat(symbol);
      for(char bit: binarySymbol.toCharArray()){
        if(bit == '1'){
          lines.set(linesIndex, lines.get(linesIndex) + ' ');
        }
        linesIndex++;
      }
    }

    return lines;
  }

  private String toBinaryFormat(byte symbol){
    String binary = Integer.toBinaryString(Byte.toUnsignedInt(symbol));
    return (binary.length()!=8? String.join("", Collections.nCopies(8-binary.length(),"0"))+ binary: binary);
  }
}
