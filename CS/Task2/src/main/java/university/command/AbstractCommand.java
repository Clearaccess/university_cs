package university.command;

import java.io.IOException;
import university.console.AbstractConsoleHandler;
import university.container.AbstractContainerManager;

public abstract class AbstractCommand {

  protected AbstractContainerManager containerManager;
  protected AbstractConsoleHandler consoleHandler;

  public AbstractCommand(AbstractContainerManager containerManager, AbstractConsoleHandler consoleHandler){

    this.containerManager = containerManager;
    this.consoleHandler = consoleHandler;
  }

  public abstract void doIt() throws IOException;
}
