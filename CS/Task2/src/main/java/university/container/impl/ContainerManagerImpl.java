package university.container.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.stream.Collectors;
import university.container.AbstractContainerManager;

public class ContainerManagerImpl extends AbstractContainerManager {


  public ContainerManagerImpl(String containerPath, String charsetName)
      throws FileNotFoundException, UnsupportedEncodingException {
    super(containerPath, charsetName);
  }

  @Override
  public List<String> loadLines() throws IOException {

    return Files.lines(containerPath, charset).collect(Collectors.toList());
  }

  @Override
  public void writeLines(List<String> lines) throws IOException {
    Files.write(containerPath, lines, charset, StandardOpenOption.WRITE);
  }

  @Override
  public void resetContainerState() throws IOException {

    List<String> lines = Files.lines(containerPath, charset)
        .map(ContainerManagerImpl::removeEndSpace).collect(Collectors.toList());
    Files.write(containerPath, lines, charset, StandardOpenOption.WRITE);
    resetIsEncoded();
  }

  private static String removeEndSpace(String line) {

    if (line.length() - 1>=0 && line.charAt(line.length() - 1) == ' ') {

      StringBuilder temp = new StringBuilder(line);
      int index = temp.length()-1;
      while(index>=0 && temp.charAt(index)==' '){
        temp = temp.deleteCharAt(index);
        index--;
      }

      return temp.toString();
    }

    return line;
  }
}
