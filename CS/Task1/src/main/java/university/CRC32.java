package university;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class CRC32 {

  private static int genCRC32code(byte[] bytes){
    int crc  = 0xFFFFFFFF;       // initial contents of LFBSR
    int poly = 0xEDB88320;   // reverse polynomial

    for (byte b : bytes) {
      int temp = (crc ^ b) & 0xff;

      // read 8 bits one at a time
      for (int i = 0; i < 8; i++) {
        if ((temp & 1) == 1) temp = (temp >>> 1) ^ poly;
        else                 temp = (temp >>> 1);
      }
      crc = (crc >>> 8) ^ temp;
    }

    // flip bits
    crc = crc ^ 0xffffffff;

    return crc;
  }

  public static void main(String[] args) {

    System.out.println("Input path to file: ");
    Scanner in = new Scanner(System.in);
    String strPath = in.next();

    if (strPath == null){
      printErrorPathFile();
      return;
    }

    Path filePath = Paths.get(strPath);
    if(Files.notExists(filePath) || Files.isDirectory(filePath)){
      printErrorPathFile();
      return;
    }

    try {
      byte[] fileBytes = Files.readAllBytes(filePath);
      int CRC32Code = genCRC32code(fileBytes);
      System.out.println(String.format("%s CRC32: %s", filePath.getFileName().toString(), Integer.toHexString(CRC32Code)));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static void printErrorPathFile(){
    System.err.println("Incorrect path to file");
  }
}
