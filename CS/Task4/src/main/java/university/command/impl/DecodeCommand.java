package university.command.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import university.command.AbstractCommand;
import university.console.AbstractConsoleHandler;
import university.container.AbstractContainerManager;
import university.util.Decoder;
import university.util.impl.DecoderImpl;

public class DecodeCommand extends AbstractCommand {

  public DecodeCommand(AbstractContainerManager containerManager,
      AbstractConsoleHandler consoleHandler) {
    super(containerManager, consoleHandler);
  }

  @Override
  public void doIt() throws IOException {

    if(!containerManager.isEncoded()){
      throw new IllegalStateException("Container doesn't hold encoded phrase");
    }

    Decoder decoder = new DecoderImpl(containerManager);
    String decodedPhrase = decoder.decode();
    consoleHandler.println(String.format("Decoded phrase %s", decodedPhrase));
  }
}
