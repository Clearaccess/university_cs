package university.command.impl;

import java.io.IOException;
import university.command.AbstractCommand;
import university.console.AbstractConsoleHandler;
import university.container.AbstractContainerManager;

public class ExitCommand extends AbstractCommand {

  public ExitCommand(AbstractContainerManager containerManager, AbstractConsoleHandler consoleHandler) {
    super(containerManager, consoleHandler);
  }

  @Override
  public void doIt() throws IOException {

    containerManager.resetContainerState();
    System.exit(0);
  }
}
