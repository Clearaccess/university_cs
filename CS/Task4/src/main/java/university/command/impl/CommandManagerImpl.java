package university.command.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import university.command.AbstractCommand;
import university.command.AbstractCommandManager;
import university.console.AbstractConsoleHandler;
import university.container.AbstractContainerManager;
import university.util.Decoder;
import university.util.Encoder;
import university.util.impl.DecoderImpl;
import university.util.impl.EncoderImpl;

public class CommandManagerImpl extends AbstractCommandManager {
  public CommandManagerImpl(AbstractContainerManager containerManager, AbstractConsoleHandler consoleHandler) {

    super(containerManager, consoleHandler);
  }

  @Override
  public void executeCommand(Commands command) throws IOException {

    AbstractCommand activeCommand = null;

    if(command.equals(Commands.ENCODE)){

      activeCommand = new EncodeCommand(containerManager, consoleHandler);

    } else if (command.equals(Commands.DECODE)){

      activeCommand = new DecodeCommand(containerManager, consoleHandler);

    } else {

      activeCommand = new ExitCommand(containerManager, consoleHandler);
    }

    activeCommand.doIt();
  }
}
