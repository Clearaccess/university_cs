package university.container;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractContainerManager {

  protected Path containerPath;
  protected Path similarDictPath;
  protected Charset charset;
  protected boolean isEncoded;
  protected long dictPower;
  protected Map<String, String> similarDictRuEn;
  protected Map<String, String> similarDictEnRu;

  public AbstractContainerManager(String containerPath, String similarDictFileName, String charsetName)
      throws IOException, URISyntaxException {

    this.charset = Charset.forName(charsetName);
    this.containerPath = Paths.get(URLDecoder.decode(containerPath, this.charset.name()));
    if(Files.notExists(this.containerPath)){
      throw new FileNotFoundException("Container path isn't correct");
    }

    this.similarDictPath = getResourcePath(similarDictFileName);
    if(Files.notExists(this.similarDictPath)){
      throw new FileNotFoundException("Similar Dict path isn't correct");
    }
    this.similarDictRuEn = loadSimilarDict(false);
    this.similarDictEnRu = loadSimilarDict(true);

    this.dictPower = calcPowerContainer();
    this.isEncoded = false;
  }

  public Charset getCharset() {
    return charset;
  }

  public Map<String, String> getSimilarDictRuEn() {
    return similarDictRuEn;
  }

  public Map<String, String> getSimilarDictEnRu() {
    return similarDictEnRu;
  }

  public long getDictPower() {
    return dictPower;
  }

  public boolean isEncoded(){
    return isEncoded;
  }

  public void setEncoded() {
    isEncoded = true;
  }

  public void resetIsEncoded(){
    isEncoded = false;
  }

  public char toCyrillicSymbol(int symbolCode){
    return new String(new byte[]{(byte)symbolCode}, charset).charAt(0);
  }

  public int nextIndex(String line, boolean ru){

    return nextIndex(line, 0, ru);
  }

  public int nextIndex(String line, int startPos, boolean ru){

    int index = -1;
    for(int i=startPos;i<line.length();i++){

      if(ru) {
        if (similarDictRuEn.containsKey(String.valueOf(line.charAt(i)))) {
          index = i;
          break;
        }
      } else {
        if (similarDictEnRu.containsKey(String.valueOf(line.charAt(i)))) {
          index = i;
          break;
        }
      }
    }

    return index;
  }

  public int nextIndex(StringBuilder line, boolean ru){

    return nextIndex(line, 0, ru);
  }

  public int nextIndex(StringBuilder line, int startPos, boolean ru){

    int index = -1;
    for(int i=startPos;i<line.length();i++){

      if(ru) {
        if (similarDictRuEn.containsKey(String.valueOf(line.charAt(i)))) {
          index = i;
          break;
        }
      } else {
        if (similarDictEnRu.containsKey(String.valueOf(line.charAt(i)))) {
          index = i;
          break;
        }
      }
    }

    return index;
  }

  public abstract List<String> loadLines() throws IOException;
  public abstract String loadAllAsLine() throws IOException;
  public abstract void writeLines(List<String>lines) throws IOException;
  public abstract void writeLine(String line) throws IOException;
  public abstract void resetContainerState() throws IOException;

  protected Path getResourcePath(String resourceName) throws URISyntaxException {
    ClassLoader loader = Thread.currentThread().getContextClassLoader();
    URI resourceURI = loader.getResource(resourceName).toURI();
    return Paths.get(resourceURI);
  }

  private Map<String, String> loadSimilarDict(boolean enRu) throws IOException {
    List<String> pairs = Files.readAllLines(similarDictPath, charset);

    Map<String, String> dict = new HashMap<>();
    for(String pair: pairs){
      String[] temp = pair.split(":");
      if(enRu) {
        dict.put(temp[0], temp[1]);
      } else {
        dict.put(temp[1], temp[0]);
      }
    }

    return dict;
  }
  private long calcPowerContainer() throws IOException {

    return Files.lines(this.containerPath, charset).mapToLong(line->line.chars().filter(ch-> similarDictRuEn
        .containsKey(Character.toString((char)ch))).count()).sum();
  }
}
