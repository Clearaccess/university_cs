package university.util.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import university.container.AbstractContainerManager;
import university.util.Decoder;

public class DecoderImpl implements Decoder {

  protected AbstractContainerManager containerManager;

  public DecoderImpl(AbstractContainerManager containerManager) {
    this.containerManager = containerManager;
  }

  @Override
  public String decode() throws IOException {

    String line = containerManager.loadAllAsLine();
    List<String> binarySymbols = extractBinarySymbols(line);
    String phrase = extractPhrase(binarySymbols);
    return phrase;
  }

  private List<String> extractBinarySymbols(String line) {

    List<String> binarySymbols = new ArrayList<>();

    StringBuilder binarySymbol = new StringBuilder();

    for (char c : line.toCharArray()) {

      if (containerManager.getSimilarDictEnRu().containsKey(String.valueOf(c))) {
        binarySymbol.append('1');
      } else if (containerManager.getSimilarDictRuEn().containsKey(String.valueOf(c))) {
        binarySymbol.append('0');
      }

      if (binarySymbol.length() == 8) {
        String binary = binarySymbol.toString();
        if (!binary.contains("1")) {
          break;
        }

        binarySymbols.add(binary);
        binarySymbol = new StringBuilder();
      }
    }

    return binarySymbols;
  }

  private String extractPhrase(List<String> binarySymbols) throws UnsupportedEncodingException {

    StringBuilder phrase = new StringBuilder();

    for (String binarySymbol : binarySymbols) {
      phrase.append(containerManager.toCyrillicSymbol(Integer.parseInt(binarySymbol, 2)));
    }

    return phrase.toString();
  }
}
