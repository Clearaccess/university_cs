package university.util.impl;

import java.io.IOException;
import java.util.Collections;
import university.container.AbstractContainerManager;
import university.util.Encoder;

public class EncoderImpl implements Encoder {

  protected AbstractContainerManager containerManager;

  public EncoderImpl(AbstractContainerManager containerManager) {
    this.containerManager = containerManager;
  }

  @Override
  public void encode(String phrase) throws IOException {

    containerManager.resetContainerState();
    String line = containerManager.loadAllAsLine();
    byte[] phraseBytes = phrase.getBytes(containerManager.getCharset());
    line = encodeToLine(line, phraseBytes);
    containerManager.writeLine(line);
    containerManager.setEncoded();
  }

  private String encodeToLine(String line, byte[] phraseBytes){

    if(containerManager.getDictPower()<phraseBytes.length*8){
      throw new IllegalArgumentException("Phrase is very long for container");
    }

    StringBuilder temp = new StringBuilder(line);
    int lineIndex = containerManager.nextIndex(temp, true);
    for(byte symbol: phraseBytes){
      String binarySymbol = toBinaryFormat(symbol);
      for(char bit: binarySymbol.toCharArray()){
        if(bit == '1'){
          temp.replace(lineIndex, lineIndex+1, containerManager.getSimilarDictRuEn().get(String.valueOf(temp.charAt(lineIndex))));
        }

        lineIndex = containerManager.nextIndex(temp, lineIndex+1, true);
      }
    }

    return temp.toString();
  }

  private String toBinaryFormat(byte symbol){
    String binary = Integer.toBinaryString(Byte.toUnsignedInt(symbol));
    return (binary.length()!=8? String.join("", Collections.nCopies(8-binary.length(),"0"))+ binary: binary);
  }
}
