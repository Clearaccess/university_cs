package university;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import university.command.AbstractCommandManager.Commands;
import university.console.AbstractConsoleHandler;
import university.console.impl.ConsoleHandlerImpl;
import university.command.AbstractCommandManager;
import university.command.impl.CommandManagerImpl;
import university.container.AbstractContainerManager;
import university.container.impl.ContainerManagerImpl;

/*
  COMMANDS:
    encode - encode phrase (input phrase -> input path to container)
    decode - decode phrase
    exit - exit from app
   */

public class App {

  protected static final String OPTIONS_PATH = "options.properties";
  protected static final String OPTIONS_CONTAINER_PATH = "container_path";
  protected static final String OPTIONS_CHARSET = "charset";
  protected static final String OPTIONS_SIMILAR_DICT_FN = "similar_dict_fn";

  protected static Properties options;
  protected static AbstractContainerManager containerManager;
  protected static AbstractCommandManager commandManager;
  protected static AbstractConsoleHandler consoleHandler;

  public static void main(String[] args) {

    try {

      init();
      while(true) {

        consoleHandler.println("Enter command ('encode', 'decode' or 'exit')...");
        String cmd = consoleHandler.readLine();
        Commands commandName = Commands.commandOf(cmd);
        commandManager.executeCommand(commandName);
      }
    } catch (IOException e) {
      e.printStackTrace();
    } catch (URISyntaxException e) {
      e.printStackTrace();
    }
  }

  private static void init() throws IOException, URISyntaxException {
    options = loadProperties();
    consoleHandler = new ConsoleHandlerImpl();
    containerManager = initContainerManager();
    commandManager = initCommandManager();
  }

  private static Properties loadProperties() throws URISyntaxException, IOException {

    ClassLoader loader = Thread.currentThread().getContextClassLoader();
    Properties properties = new Properties();

    try(InputStream resourceStream = loader.getResourceAsStream(OPTIONS_PATH)) {
      properties.load(resourceStream);
    }

    return properties;
  }

  private static AbstractContainerManager initContainerManager()
      throws IOException, URISyntaxException {

    return new ContainerManagerImpl(options.getProperty(OPTIONS_CONTAINER_PATH), options.getProperty(OPTIONS_SIMILAR_DICT_FN),options.getProperty(OPTIONS_CHARSET));
  }

  private static AbstractCommandManager initCommandManager(){

    return new CommandManagerImpl(containerManager, consoleHandler);
  }
}
