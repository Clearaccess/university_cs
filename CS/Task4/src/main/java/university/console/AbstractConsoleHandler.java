package university.console;

import java.util.Scanner;

public abstract class AbstractConsoleHandler {

  protected Scanner scanner;

  public AbstractConsoleHandler() {
    this.scanner = new Scanner(System.in);
  }

  public abstract void println(String out);

  public abstract String readLine();

}
